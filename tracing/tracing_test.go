package tracing

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestTracing(t *testing.T) {
	h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	})
	httpSrv := httptest.NewServer(h)
	defer httpSrv.Close()

	tmpf, err := ioutil.TempFile("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(tmpf.Name())
	defer tmpf.Close()

	if err := json.NewEncoder(tmpf).Encode(&tracingConfig{
		ReportURL: httpSrv.URL,
		Sample:    "1.0",
	}); err != nil {
		t.Fatal(err)
	}

	os.Setenv("TRACING_ENABLE", "1")
	os.Setenv("TRACING_CONFIG", tmpf.Name())

	Init()

	if !Enabled {
		t.Fatal("tracing not enabled")
	}
}
