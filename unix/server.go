package unix

import (
	"bufio"
	"container/list"
	"context"
	"errors"
	"io"
	"log"
	"net"
	"net/textproto"
	"os"
	"sync"
	"sync/atomic"
	"time"

	"github.com/coreos/go-systemd/v22/activation"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/gofrs/flock"
)

// Handler for UNIX socket server connections.
type Handler interface {
	ServeConnection(c net.Conn)
}

// SocketServer accepts connections on a UNIX socket, speaking the
// line-based wire protocol, and dispatches incoming requests to the
// wrapped Server.
type SocketServer struct {
	l       net.Listener
	lock    *flock.Flock
	closing atomic.Value
	wg      sync.WaitGroup
	handler Handler

	// Keep track of active connections so we can shut them down
	// on Close.
	connMx sync.Mutex
	conns  list.List
}

func newServer(l net.Listener, lock *flock.Flock, h Handler) *SocketServer {
	s := &SocketServer{
		l:       l,
		lock:    lock,
		handler: h,
	}
	s.closing.Store(false)
	return s
}

// NewUNIXSocketServer returns a new SocketServer listening on the given path.
func NewUNIXSocketServer(socketPath string, h Handler) (*SocketServer, error) {
	// The simplest workflow is: create a new socket, remove it on
	// exit. However, if the program crashes, the socket might
	// stick around and prevent the next execution from starting
	// successfully. We could remove it before starting, but that
	// would be dangerous if another instance was listening on
	// that socket. So we wrap socket access with a file lock.
	lock := flock.New(socketPath + ".lock")
	locked, err := lock.TryLock()
	if err != nil {
		return nil, err
	}
	if !locked {
		return nil, errors.New("socket is locked by another process")
	}

	addr, err := net.ResolveUnixAddr("unix", socketPath)
	if err != nil {
		return nil, err
	}

	// Always try to unlink the socket before creating it.
	os.Remove(socketPath) // nolint

	l, err := net.ListenUnix("unix", addr)
	if err != nil {
		return nil, err
	}

	return newServer(l, lock, h), nil
}

// NewSystemdSocketServer uses systemd socket activation, receiving
// the open socket as a file descriptor on exec.
func NewSystemdSocketServer(h Handler) (*SocketServer, error) {
	listeners, err := activation.Listeners()
	if err != nil {
		return nil, err
	}
	// Our server loop implies a single listener, so find
	// the first one passed by systemd and ignore all others.
	// TODO: listen on all fds.
	for _, l := range listeners {
		if l != nil {
			return newServer(l, nil, h), nil
		}
	}
	return nil, errors.New("no available sockets found")
}

// Close the socket listener and release all associated resources.
// Waits for active connections to terminate before returning.
func (s *SocketServer) Close() {
	s.closing.Store(true)

	// Close the listener to stop incoming connections.
	s.l.Close() // nolint

	// Close all active connections (this will return an error to
	// the client if the connection is not idle).
	s.connMx.Lock()
	for el := s.conns.Front(); el != nil; el = el.Next() {
		el.Value.(net.Conn).Close() // nolint
	}
	s.connMx.Unlock()

	s.wg.Wait()
	if s.lock != nil {
		s.lock.Unlock() // nolint
	}
}

func (s *SocketServer) isClosing() bool {
	return s.closing.Load().(bool)
}

// Serve connections.
func (s *SocketServer) Serve() error {
	for {
		conn, err := s.l.Accept()
		if err != nil {
			if s.isClosing() {
				return nil
			}
			return err
		}

		s.wg.Add(1)

		s.connMx.Lock()
		connEl := s.conns.PushBack(conn)
		s.connMx.Unlock()

		go func() {
			s.handler.ServeConnection(conn)
			conn.Close() // nolint
			if !s.isClosing() {
				s.connMx.Lock()
				s.conns.Remove(connEl)
				s.connMx.Unlock()
			}
			s.wg.Done()
		}()
	}
}

// LineHandler is the handler for LineServer.
type LineHandler interface {
	ServeLine(context.Context, LineResponseWriter, []byte) error
}

// ErrCloseConnection must be returned by a LineHandler when we want
// to cleanly terminate the connection without raising an error.
var ErrCloseConnection = errors.New("close")

// LineResponseWriter writes a single-line response to the underlying
// connection.
type LineResponseWriter interface {
	// WriteLine writes a response (which must include the
	// line terminator).
	WriteLine([]byte) error

	// WriteLineCRLF writes a response and adds a line terminator.
	WriteLineCRLF([]byte) error
}

// LineServer implements a line-based text protocol. It satisfies the
// Handler interface.
type LineServer struct {
	handler LineHandler

	IdleTimeout    time.Duration
	WriteTimeout   time.Duration
	RequestTimeout time.Duration
}

var (
	defaultIdleTimeout    = 600 * time.Second
	defaultWriteTimeout   = 10 * time.Second
	defaultRequestTimeout = 30 * time.Second
)

// NewLineServer returns a new LineServer with the given handler and
// default I/O timeouts.
func NewLineServer(h LineHandler) *LineServer {
	return &LineServer{
		handler:        h,
		IdleTimeout:    defaultIdleTimeout,
		WriteTimeout:   defaultWriteTimeout,
		RequestTimeout: defaultRequestTimeout,
	}
}

var crlf = []byte{'\r', '\n'}

type lrWriter struct {
	*bufio.Writer
}

func (w *lrWriter) WriteLine(data []byte) error {
	if _, err := w.Writer.Write(data); err != nil {
		return err
	}
	return w.Writer.Flush()
}

func (w *lrWriter) WriteLineCRLF(data []byte) error {
	if _, err := w.Writer.Write(data); err != nil {
		return err
	}
	if _, err := w.Writer.Write(crlf); err != nil {
		return err
	}
	return w.Writer.Flush()
}

// ServeConnection handles a new connection. It will accept multiple
// requests on the same connection (or not, depending on the client
// preference).
func (l *LineServer) ServeConnection(nc net.Conn) {
	totalConnections.Inc()
	c := textproto.NewConn(nc)
	rw := &lrWriter{bufio.NewWriter(nc)}
	for {
		nc.SetReadDeadline(time.Now().Add(l.IdleTimeout)) // nolint
		line, err := c.ReadLineBytes()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Printf("client error: %v", err)
			break
		}

		// Create a context for the request and call the
		// handler with it.  Set a write deadline on the
		// connection to allow the full RequestTimeout time to
		// generate the response.
		start := time.Now()
		nc.SetWriteDeadline(start.Add(l.RequestTimeout + l.WriteTimeout)) // nolint
		ctx, cancel := context.WithTimeout(context.Background(), l.RequestTimeout)
		err = l.handler.ServeLine(ctx, rw, line)
		elapsedMs := time.Since(start).Nanoseconds() / 1000000
		requestLatencyHist.Observe(float64(elapsedMs))
		cancel()

		// Close the connection on error, or on empty response.
		if err != nil {
			totalRequests.With(prometheus.Labels{
				"status": "error",
			}).Inc()
			if err != ErrCloseConnection {
				log.Printf("request error: %v", err)
			}
			break
		}
		totalRequests.With(prometheus.Labels{
			"status": "ok",
		}).Inc()
	}
}

// Instrumentation metrics.
var (
	totalConnections = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "unix_connections_total",
			Help: "Total number of connections.",
		},
	)
	totalRequests = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "unix_requests_total",
			Help: "Total number of requests.",
		},
		[]string{"status"},
	)
	// Histogram buckets are tuned for the low-milliseconds range
	// (the largest bucket sits at ~1s).
	requestLatencyHist = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:    "unix_requests_latency_ms",
			Help:    "Latency of requests.",
			Buckets: prometheus.ExponentialBuckets(5, 1.4142, 16),
		},
	)
)

func init() {
	prometheus.MustRegister(totalConnections)
	prometheus.MustRegister(totalRequests)
	prometheus.MustRegister(requestLatencyHist)

}
