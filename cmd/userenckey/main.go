package main

// Encrypt/decrypt user storage encryption keys.
//
// Keys are always in the composite format used in our LDAP backend
// (<id>:<key>), base64-encoded for convenience (and for compatibility
// with LDIF binary field encoding).

import (
	"bytes"
	"encoding/base64"
	"flag"
	"fmt"
	"log"

	"git.autistici.org/ai3/go-common/userenckey"
)

var (
	doGenKeys = flag.Bool("gen-keys", false, "generate user encryption keys with the specified curve")
	doDecrypt = flag.Bool("decrypt", false, "decrypt the private key given on the command line")
	password  = flag.String("password", "", "password")
	keyID     = flag.String("id", "", "key ID")
)

func genKeys() ([]byte, []byte, error) {
	pub, priv, err := userenckey.GenerateKey()
	if err != nil {
		return nil, nil, err
	}

	enc, err := userenckey.Encrypt(priv, []byte(*password))
	if err != nil {
		return nil, nil, err
	}

	return enc, pub, err
}

func encodeWithID(key []byte) []byte {
	return bytes.Join([][]byte{[]byte(*keyID), key}, []byte(":"))
}

func decodeWithoutID(enc []byte) []byte {
	if n := bytes.IndexByte(enc, ':'); n > 0 {
		enc = enc[n+1:]
	}
	return enc
}

func printLDAPField(key string, value []byte) {
	fmt.Printf("%s:: %s\n", key, base64.StdEncoding.EncodeToString(value))
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	switch {
	case *doGenKeys:
		if *password == "" || *keyID == "" {
			log.Fatal("must specify --password and --id")
		}

		priv, pub, err := genKeys()
		if err != nil {
			log.Fatal(err)
		}
		printLDAPField("storagePublicKey", pub)
		printLDAPField("storageEncryptedSecretKey", encodeWithID(priv))

	case *doDecrypt:
		if *password == "" {
			log.Fatal("must specify --password")
		}
		if flag.NArg() < 1 {
			log.Fatal("not enough arguments")
		}

		var encKeys [][]byte
		for _, arg := range flag.Args() {
			encKey, err := base64.StdEncoding.DecodeString(arg)
			if err != nil {
				log.Fatalf("bad base64-encoded argument: %v", err)
			}
			encKeys = append(encKeys, decodeWithoutID(encKey))
		}

		dec, err := userenckey.Decrypt(encKeys, []byte(*password))
		if err != nil {
			log.Fatal(err)
		}
		pem, err := dec.PEM()
		if err != nil {
			log.Fatalf("invalid private key: %v", err)
		}
		fmt.Printf("private key:\n%s\n", pem)

	default:
		log.Fatal("no actions specified")
	}
}
