package pwhash

import (
	"fmt"
	"testing"
)

func TestArgon2Legacy(t *testing.T) {
	testImpl(t, NewArgon2Legacy())
}

func TestArgon2Std(t *testing.T) {
	testImpl(t, NewArgon2StdWithParams(3, 4096, 2))
}

func TestScrypt(t *testing.T) {
	testImpl(t, NewScrypt())
}

func TestSystemCrypt(t *testing.T) {
	testImpl(t, NewSystemCrypt())
}

func TestDefault(t *testing.T) {
	testImpl(t, nil)
}

func testImpl(t *testing.T, h PasswordHash) {
	pw1 := "password 1"
	pw2 := "password 2"

	var enc1, enc2 string
	if h == nil {
		enc1 = Encrypt(pw1)
		enc2 = Encrypt(pw2)
	} else {
		enc1 = h.Encrypt(pw1)
		enc2 = h.Encrypt(pw2)
	}
	//t.Logf("enc1=%s", enc1)

	testData := []struct {
		enc            string
		pw             string
		expectedResult bool
	}{
		{enc1, pw1, true},
		{enc2, pw2, true},
		{enc1, pw2, false},
		{enc2, pw1, false},
		{enc1, "", false},
		{enc1, "foo", false},
	}

	for _, td := range testData {
		var result bool
		if h == nil {
			result = ComparePassword(td.enc, td.pw)
		} else {
			result = h.ComparePassword(td.enc, td.pw)
		}
		if result != td.expectedResult {
			t.Errorf("compare(%s, %s): got %v, expected %v", td.enc, td.pw, result, td.expectedResult)
		}
	}
}

func TestStandardArgon2IPassword(t *testing.T) {
	enc := "$argon2i$v=19$m=32768,t=4,p=1$DG0B56zlrrx+VMVaM6wvsw$8iV+HwTKmofjrb+q9I2zZGQnGXzXtiIXv8VdHdvbbX8"
	pw := "idontmindbirds"
	if !ComparePassword(enc, pw) {
		t.Fatal("comparison failed")
	}
}

func TestStandardArgon2IDPassword(t *testing.T) {
	// python3 -c 'from argon2 import PasswordHasher ; print(PasswordHasher().hash("idontmindbirds"))'
	enc := "$argon2id$v=19$m=102400,t=2,p=8$7hQLBrHoxYxRO0R8km62pA$Dv5+BCctW4nCrxsy5C9JBg"
	pw := "idontmindbirds"
	if !ComparePassword(enc, pw) {
		t.Fatal("comparison failed")
	}
}

func BenchmarkArgon2(b *testing.B) {
	var testParams []argon2Params
	for iTime := 1; iTime <= 5; iTime++ {
		for iThreads := 1; iThreads <= 8; iThreads *= 2 {
			for iMem := 1; iMem <= 16; iMem *= 2 {
				testParams = append(testParams, argon2Params{
					Time:    uint32(iTime),
					Memory:  uint32(iMem * 1024),
					Threads: uint8(iThreads),
				})
			}
		}
	}

	goodPw := "good password"
	badPw := "definitely not the good password"

	for _, tp := range testParams {
		name := fmt.Sprintf("%d/%d/%d", tp.Time, tp.Memory, tp.Threads)
		b.Run(name, func(b *testing.B) {
			h := NewArgon2StdWithParams(tp.Time, tp.Memory, tp.Threads)
			encPw := h.Encrypt(goodPw)

			b.ResetTimer()
			for i := 0; i < b.N; i++ {
				h.ComparePassword(encPw, badPw) // nolint
			}
		})
	}
}
