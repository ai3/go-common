package message

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"mime"
	"mime/multipart"
	"mime/quotedprintable"
	"net/textproto"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/emersion/go-textwrapper"
)

var (
	mimeDefaultBody = []byte("This is a multi-part message in MIME format.\r\n")
)

// Middleware is an interface for something that modifies a message
// (identified by the top-level Part).
type Middleware interface {
	Process(*Part) (*Part, error)
}

// MiddlewareList is a list of Middleware instances.
type MiddlewareList []Middleware

// Add a Middleware to the list.
func (l *MiddlewareList) Add(m Middleware) {
	*l = append(*l, m)
}

// Process a message. Implements the Middleware interface.
func (l MiddlewareList) Process(part *Part) (*Part, error) {
	for _, m := range l {
		var err error
		part, err = m.Process(part)
		if err != nil {
			return nil, err
		}
	}
	return part, nil
}

// Part is a MIME multipart entity. It can contain a body, or
// sub-parts. Use the New* methods to create one. An email message is
// represented by a tree of Part objects.
type Part struct {
	Header   textproto.MIMEHeader
	Body     []byte
	Subparts []*Part
	boundary string
}

// NewPart creates a new Part with the given header and body.
func NewPart(hdr textproto.MIMEHeader, body []byte) *Part {
	return &Part{
		Header: hdr,
		Body:   body,
	}
}

// NewMultiPart creates a multipart entity. The content-type must be
// manually specified and must start with "multipart/".
func NewMultiPart(ctype string, parts ...*Part) *Part {
	boundary := RandomBoundaryFn()
	return &Part{
		Header: textproto.MIMEHeader{
			"Content-Type": []string{fmt.Sprintf("%s; boundary=\"%s\"", ctype, boundary)},
		},
		Body:     mimeDefaultBody,
		Subparts: parts,
		boundary: boundary,
	}
}

// NewText creates a text MIME part. Charset is assumed to be UTF-8,
// and quoted-printable encoding is used.
func NewText(ctype string, body []byte) *Part {
	return &Part{
		Header: textproto.MIMEHeader{
			"Content-Type":              []string{fmt.Sprintf("%s; charset=UTF-8", ctype)},
			"Content-Transfer-Encoding": []string{"quoted-printable"},
		},
		Body: quopri(body),
	}
}

// NewAttachment creates a MIME multipart object representing a file
// attachment. The filename is the desired name for the object in MIME
// headers, while path points at the local filesystem path.
func NewAttachment(filename, ctype, path string) (*Part, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var buf bytes.Buffer
	enc := base64.NewEncoder(base64.StdEncoding, textwrapper.NewRFC822(&buf))
	if _, err := io.Copy(enc, f); err != nil {
		return nil, err
	}

	// Autodetect content-type if empty.
	if ctype == "" {
		ctype = mime.TypeByExtension(filepath.Ext(filename))
	}
	if ctype == "" {
		ctype = "application/octet-stream"
	}

	return &Part{
		Header: textproto.MIMEHeader{
			"Content-Type":              []string{fmt.Sprintf("%s; name=\"%s\"", ctype, filename)},
			"Content-Disposition":       []string{fmt.Sprintf("attachment; filename=\"%s\"", filename)},
			"Content-Transfer-Encoding": []string{"base64"},
		},
		Body: buf.Bytes(),
	}, nil
}

// Add a sub-Part to this object.
func (p *Part) Add(subp *Part) error {
	if !p.isMultipart() {
		return errors.New("not a multipart container")
	}
	p.Subparts = append(p.Subparts, subp)
	return nil
}

func (p *Part) isMultipart() bool {
	return strings.HasPrefix(p.Header.Get("Content-Type"), "multipart/")
}

type strList []string

func (l strList) Len() int           { return len(l) }
func (l strList) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
func (l strList) Less(i, j int) bool { return l[i] < l[j] }

func (p *Part) writeHeader(w io.Writer) {
	// Sort the keys for stable output.
	var keys []string
	for k := range p.Header {
		keys = append(keys, k)
	}
	sort.Sort(strList(keys))

	for _, k := range keys {
		for _, v := range p.Header[k] {
			fmt.Fprintf(w, "%s: %s\r\n", k, v)
		}
	}
	io.WriteString(w, "\r\n") // nolint
}

func (p *Part) render(w io.Writer, writeHeader bool) error {
	if writeHeader {
		p.writeHeader(w)
	}
	if _, err := w.Write(p.Body); err != nil {
		return err
	}
	if p.isMultipart() {
		mw := multipart.NewWriter(w)
		if err := mw.SetBoundary(p.boundary); err != nil {
			return err
		}
		for _, sub := range p.Subparts {
			pw, err := mw.CreatePart(sub.Header)
			if err != nil {
				return err
			}
			if err := sub.render(pw, false); err != nil {
				return err
			}
		}
		mw.Close()
	}
	return nil
}

// Render the message to an io.Writer.
func (p *Part) Render(w io.Writer) error {
	return p.render(w, true)
}

// func (p *Part) String() string {
// 	var buf bytes.Buffer
// 	if err := p.render(&buf, true); err != nil {
// 		return "<error>"
// 	}
// 	return buf.String()
// }

// RandomBoundaryFn points at the function used to generate MIME
// boundaries (by default RandomBoundary). Allows us to stub it out
// for testing.
var RandomBoundaryFn func() string = RandomBoundary

// RandomBoundary returns a pseudorandom sequence of bytes that is
// suitable for a MIME boundary.
func RandomBoundary() string {
	buf := make([]byte, 30)
	_, err := io.ReadFull(rand.Reader, buf)
	if err != nil {
		panic(err)
	}
	return fmt.Sprintf("%x", buf)
}

func quopri(b []byte) []byte {
	var buf bytes.Buffer
	qp := quotedprintable.NewWriter(&buf)
	qp.Write(b) // nolint
	qp.Close()
	return buf.Bytes()
}
