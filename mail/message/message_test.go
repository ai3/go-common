package message

import "testing"

func TestRandomBoundary(t *testing.T) {
	s := RandomBoundary()
	if len(s) < 30 {
		t.Errorf("boundary too short: %s", s)
	}
}
