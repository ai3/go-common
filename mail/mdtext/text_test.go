package mdtext

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	bf "github.com/russross/blackfriday/v2"
)

var (
	testText = `
doctitle
========

This is a *Markdown* test document that is meant to be rendered to text, with the ultimate purpose of generating nicely formatted email templates.

* list element one
  * nested list element one, whose only purpose is to have a very long message so we can see how (and *if*) the text wraps around onto multiple lines with the proper indentation
  * nested list element two
* list element two
* list element three
* more information on [wikipedia](https://wikipedia.org/).

## Level 2 header

Yet another long paragraph to showcase the word wrapping capabilities of the software.
In theory the long lines should be wrapped at around 75 characters, so as to generate nicely
formatted emails (the original text is wrapped at 100 chars for the purpose of showcasing this
specific feature.

> This is a block quote, and as such we would like to see it indented by four spaces, but also
> maintaining the proper indentation on the lines following the first one, which is the feature
> tested by this snippet.

    Code blocks, on the other hand, should have no line wrapping and should keep their own formatting.

Finally another paragraph to conclude this test.

`

	testTextExpected60 = `# doctitle

This is a *Markdown* test document that is meant to be
rendered to text, with the ultimate purpose of generating
nicely formatted email templates.

* list element one

  * nested list element one, whose only purpose is to have a
    very long message so we can see how (and *if*) the text
    wraps around onto multiple lines with the proper
    indentation

  * nested list element two

* list element two

* list element three

* more information on https://wikipedia.org/.

## Level 2 header

Yet another long paragraph to showcase the word wrapping
capabilities of the software. In theory the long lines
should be wrapped at around 75 characters, so as to generate
nicely formatted emails (the original text is wrapped at 100
chars for the purpose of showcasing this specific feature.

> This is a block quote, and as such we would like to see it
> indented by four spaces, but also maintaining the proper
> indentation on the lines following the first one, which is
> the feature tested by this snippet.

    Code blocks, on the other hand, should have no line wrapping and should keep their own formatting.

Finally another paragraph to conclude this test.

`

	testTextExpected40 = `# doctitle

This is a *Markdown* test document that
is meant to be rendered to text, with
the ultimate purpose of generating
nicely formatted email templates.

* list element one

  * nested list element one, whose only
    purpose is to have a very long
    message so we can see how (and *if*)
    the text wraps around onto multiple
    lines with the proper indentation

  * nested list element two

* list element two

* list element three

* more information on
  https://wikipedia.org/.

## Level 2 header

Yet another long paragraph to showcase
the word wrapping capabilities of the
software. In theory the long lines
should be wrapped at around 75
characters, so as to generate nicely
formatted emails (the original text is
wrapped at 100 chars for the purpose of
showcasing this specific feature.

> This is a block quote, and as such we
> would like to see it indented by four
> spaces, but also maintaining the
> proper indentation on the lines
> following the first one, which is the
> feature tested by this snippet.

    Code blocks, on the other hand, should have no line wrapping and should keep their own formatting.

Finally another paragraph to conclude
this test.

`
)

func runTest(t *testing.T, width int, expected string) {
	r := NewTextRenderer(width)
	output := string(bf.Run([]byte(testText), bf.WithRenderer(r)))
	if diffs := cmp.Diff(expected, output); diffs != "" {
		t.Errorf("mismatched rendered output:\n%s", diffs)
	}
	t.Logf("result:\n%s", output)
}

func Test_Text_60(t *testing.T) {
	runTest(t, 60, testTextExpected60)
}

func Test_Text_40(t *testing.T) {
	runTest(t, 40, testTextExpected40)
}
