package mail

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"

	"git.autistici.org/ai3/go-common/mail/message"
	"git.autistici.org/ai3/go-common/mail/template"
)

func setupTestEnv(t testing.TB, files map[string]string) (string, func()) {
	tmpdir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}

	for path, contents := range files {
		if err := ioutil.WriteFile(filepath.Join(tmpdir, path), []byte(contents), 0600); err != nil {
			t.Fatal(err)
		}
	}

	template.SetTemplateDirectory(tmpdir)
	return tmpdir, func() {
		os.RemoveAll(tmpdir)
	}
}

func TestMail_Template(t *testing.T) {
	_, cleanup := setupTestEnv(t, map[string]string{
		"testmsg.en.md": "value: {{.value}}",
	})
	defer cleanup()

	m, err := New(&Config{
		SenderAddr: "me@localhost",
	})
	if err != nil {
		t.Fatalf("New(): %v", err)
	}

	tpl, err := template.New("testmsg", "en", map[string]interface{}{
		"value": 42,
	})
	if err != nil {
		t.Fatalf("template.New(): %v", err)
	}
	txt := message.NewText("text/plain", tpl.Text())

	msg, err := m.WithEnvelope(txt, "you@localhost", "Hello")
	if err != nil {
		t.Fatalf("Mailer.Envelope(): %v", err)
	}

	s, err := m.Render(msg)
	if err != nil {
		t.Fatalf("Mailer.Render(): %v", err)
	}

	expected := strings.Replace(`Content-Transfer-Encoding: quoted-printable
Content-Type: text/plain; charset=UTF-8
Date: Fri, 21 Nov 1997 09:55:06 -0600
From: <me@localhost>
Message-Id: <xxxxxx@localhost>
Mime-Version: 1.0
Subject: Hello
To: <you@localhost>
User-Agent: go-mailer/0.1

value: 42

`, "\n", "\r\n", -1)
	if diffs := diffStr(expected, s); diffs != "" {
		t.Errorf("unexpected output:\n%s", diffs)
	}
}

func TestMail_TemplateMultipartAlternative(t *testing.T) {
	_, cleanup := setupTestEnv(t, map[string]string{
		"testmsg.en.md": "value: {{.value}}",
	})
	defer cleanup()

	m, err := New(&Config{
		SenderAddr: "me@localhost",
	})
	if err != nil {
		t.Fatalf("New(): %v", err)
	}

	tpl, err := template.New("testmsg", "en", map[string]interface{}{
		"value": 42,
	})
	if err != nil {
		t.Fatalf("template.New(): %v", err)
	}
	txt1 := message.NewText("text/plain", tpl.Text())
	txt2 := message.NewText("text/html", tpl.HTML())

	mm := message.NewMultiPart("multipart/alternative", txt1, txt2)

	msg, err := m.WithEnvelope(mm, "you@localhost", "Hello")
	if err != nil {
		t.Fatalf("Mailer.WithEnvelope(): %v", err)
	}

	s, err := m.Render(msg)
	if err != nil {
		t.Fatalf("Mailer.Render(): %v", err)
	}

	expected := strings.Replace(`Content-Type: multipart/alternative; boundary="xxxxxx"
Date: Fri, 21 Nov 1997 09:55:06 -0600
From: <me@localhost>
Message-Id: <xxxxxx@localhost>
Mime-Version: 1.0
Subject: Hello
To: <you@localhost>
User-Agent: go-mailer/0.1

This is a multi-part message in MIME format.
--xxxxxx
Content-Transfer-Encoding: quoted-printable
Content-Type: text/plain; charset=UTF-8

value: 42


--xxxxxx
Content-Transfer-Encoding: quoted-printable
Content-Type: text/html; charset=UTF-8

<p>value: 42</p>

--xxxxxx--
`, "\n", "\r\n", -1)
	if diffs := diffStr(expected, s); diffs != "" {
		t.Errorf("unexpected output:\n%s", diffs)
	}
}

func TestMail_TemplateMultipartMixed(t *testing.T) {
	dir, cleanup := setupTestEnv(t, map[string]string{
		"testmsg.en.md":  "value: {{.value}}",
		"attachment.gif": "GIF89abcdef",
	})
	defer cleanup()

	m, err := New(&Config{
		SenderAddr: "me@localhost",
	})
	if err != nil {
		t.Fatalf("New(): %v", err)
	}

	tpl, err := template.New("testmsg", "en", map[string]interface{}{
		"value": 42,
	})
	if err != nil {
		t.Fatalf("template.New(): %v", err)
	}
	txt1 := message.NewText("text/plain", tpl.Text())

	att1, err := message.NewAttachment("attachment.gif", "", filepath.Join(dir, "attachment.gif"))
	if err != nil {
		t.Fatalf("message.NewAttachment(): %v", err)
	}

	mm := message.NewMultiPart("multipart/mixed", txt1, att1)

	msg, err := m.WithEnvelope(mm, "you@localhost", "Hello")
	if err != nil {
		t.Fatalf("Mailer.WithEnvelope(): %v", err)
	}

	s, err := m.Render(msg)
	if err != nil {
		t.Fatalf("Mailer.Render(): %v", err)
	}

	expected := strings.Replace(`Content-Type: multipart/mixed; boundary="xxxxxx"
Date: Fri, 21 Nov 1997 09:55:06 -0600
From: <me@localhost>
Message-Id: <xxxxxx@localhost>
Mime-Version: 1.0
Subject: Hello
To: <you@localhost>
User-Agent: go-mailer/0.1

This is a multi-part message in MIME format.
--xxxxxx
Content-Transfer-Encoding: quoted-printable
Content-Type: text/plain; charset=UTF-8

value: 42


--xxxxxx
Content-Disposition: attachment; filename="attachment.gif"
Content-Transfer-Encoding: base64
Content-Type: image/gif; name="attachment.gif"

R0lGODlhYmNk
--xxxxxx--
`, "\n", "\r\n", -1)
	if diffs := diffStr(expected, s); diffs != "" {
		t.Errorf("unexpected output:\n%s", diffs)
	}
}

func TestMail_PGP(t *testing.T) {
	dir, cleanup := setupTestEnv(t, map[string]string{
		"testmsg.en.md": "value: {{.value}}",
		"secretkey":     testPGPKey,
	})
	defer cleanup()

	m, err := New(&Config{
		SenderAddr:     "me@localhost",
		SigningKeyFile: filepath.Join(dir, "secretkey"),
		SigningKeyID:   testPGPKeyID,
	})
	if err != nil {
		t.Fatalf("New(): %v", err)
	}

	tpl, err := template.New("testmsg", "en", map[string]interface{}{
		"value": 42,
	})
	if err != nil {
		t.Fatalf("template.New(): %v", err)
	}
	txt := message.NewText("text/plain", tpl.Text())

	msg, err := m.WithEnvelope(txt, "you@localhost", "Hello")
	if err != nil {
		t.Fatalf("Mailer.Envelope(): %v", err)
	}

	s, err := m.Render(msg)
	if err != nil {
		t.Fatalf("Mailer.Render(): %v", err)
	}

	// It's hard to actually verify the signature reliably (we
	// should use some third-party method for that) - let's just
	// check that there *is* a signature...
	if !strings.Contains(s, "-----BEGIN PGP SIGNATURE-----") {
		t.Error("the message does not seem to contain a signature")
	}
	t.Logf("\n%s", s)
}

var (
	testPGPKeyID = "CB20487E357C7966"
	testPGPKey   = `-----BEGIN PGP PRIVATE KEY BLOCK-----

lQVYBF1VycMBDACnhoq8UvRbVn+GlzrhFFidmtMvfystbcbxRyvX7ueESFdCz6Pd
EZq0mnrhlaDF5jqvt7w/4zNWUIgY+YM8aTyR/zFRiX9bdZYT+EdSE1E+8AUhRVjz
ZdcktXdUIJAosl7WCJX63R6nmzZzEJYa20Ej/XhU3F/FfBSv42omAl1sYYMaL0LY
VAaRiMlUmg4AT4Bf9ogU6XBFc0O2BEOKRZq260X+u9S985FeUH1GdrevzNDRmq2a
24VBMxXye0hjKBTJZkCpu2VgVAOUfpy1yh/ZrK1hlWH4LAvgSzt3QbAP8hIwPdSl
Kaly6QB+gCgypqNHAejMS49arJtbsk/Mt64IYyGbWWdoU0oM4i4JRgGI041vwiV4
vYHjMvaKhuhJWmXQQvcd0N/uvqhSk8ohUs4zVebWSx0SkDAdyY40g2foabWPXcV8
f3cakhY8ZCicFPCtXkoyx9ZOer8cHdoPdxn1cXXDEngVZuHpeQVz4rLbneZ0cIvk
OOyNkvWmvAdUNQ0AEQEAAQAL/RQ8x5O6fbRu/ZbXvBAmshHP+0UYmrGxOkA5dc1v
Gd68EnaKuOPi1YqNwtxvg+2EQ4CotIAPRUtfDSHfOoBYwi1s45tS/eShjtC4xHzg
wobU3fnH89frbJMNrO2nxWJ1McmvXdbhUWuz7171GP0DkZn0a83slVE5DRK2aUNQ
M9L88KaAIRYbCHQaTx/+QES/VeXB1WyZSqvJIdviJfqVL/x67Yi5ThjoTJ5VIN0b
SFNfbbZ0dhZoAHAA6NzTEcqQs8gMwF0WdTrsq6wVnVoPj4And1wXIDkeuRMBHXpk
wv/u17Rflb81UI+kkxyzZHvlFoZe1R4D8tv0Tt+yQ2Bbq853sWMWfKjw8kkfUCnw
ZPRHjGaSE/mjjUalmj5183JclD9r64+pUfoLSRcEaSX78ObRY5XSy7g1jpFb91iB
ucigu2I4n9Ays3UmIkVRo83zKHnTJxSHxCsskeXeseIqfl7rOxTcTWeolcsnoIyU
+qb8RdjDiFRIj8r8ZJYNkTJXwQYAxU0orcGUQwF9//BLXe+rIVQQ6OG2sgShjZz9
7krxtDMem2FDbL7g3jTNqDjMt9JVgEX2Kva/sXc7BKCYTx4jwlxRq4AXu5yMNji0
HHgR0EzdDr+1hJ/RaKi9vuVZmIVApJ5lM5QMnSxvsjXuIg3B+Qh+fXKu9SFoXTte
+wvuRpuLMRJ/MzZysq+PoHbYPe9iSJWpCLLE388JUPBN20KXt2/rx6tA9UY7/J+7
qpfj1sCTdhvwQlV6+Y0/Vpq9JthRBgDZXZllZTFVIvgnH5ZCtXIswUO5lVxNhzgV
G5VXe5jsfA+kRriDwb23r76EdCLRmWhGza8mpfovbXvjye3897piXmGlzgCA6Xf1
lnQpQPUIzIL54E5E58GsuoCUAfwameMLXdpT4aavE7ApMbdQ4a89Gy11D94wiLh1
z5OL0qbboweTrf5gvoDOJJLZfySZSh0nx1bV2nQfQ2S/KYvJvpijKg4qogZlrnT6
SWeoU0Xip/GZCyODE4YogDZ595nrXf0GANSz7+Y22f+V9Axq19erRi2+pv6nTabP
MGV+X+S3iSZaaSNMBhTvaBBGLLwjGiY0uikQ+Wei5CyAAdiX+eRz4Us+LM2vsUd9
381MP1qi8EYdWLBt3R5Zd2NZQwfjhgWxLgNDsUQAc3pRUwm8TB2P5W2uRRkBe9lE
1/IQgYkDxhdwglALblfWoSHYh240veSa3ukzvQIS4MgKzKxsV9v8T3333zyMQ9Cp
y4OCDhMLB/5yQImhqlMeAEepxZcaa/JPPuqPtB90ZXN0IGFjY291bnQgPHRlc3RA
ZXhhbXBsZS5jb20+iQHUBBMBCgA+FiEE9RkgwntcJS8xYiJdyyBIfjV8eWYFAl1V
ycMCGwMFCQPCZwAFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AACgkQyyBIfjV8eWYb
egv+OHu5MhvBImgZC+Z0ctNKtEByiI7EwMQUMABIprIuE4GxqHwwJgc/mrLTcH3T
CyjRhkfSpQduYjLpAep5E9sPnkDzZ8leHy+hYi/6G5WpsprC1OpH6PaVoqbYq/JB
rLPOWu1rxsCD4L1EXsR3JfhOuBoywcCTGt+g0pb3Q1LgVUM2MXiDAJmsP8rvAazE
ajP3hBPTpl0j5y1Qeyxn4qX7JezhHcRrwalNfdE3FLN+j3fLOfV6Q37D5FL2AnHi
PIPiJOaXRgiqGRyqnrAnpqMJUgF+DMekr47/NuSZoGnsYa/tQWWkLFgcfnoaAqQ1
ixR84WT5j1pP/1203NDzb5Del1Bxf5jXB9uVn9brjgHjrK1lSzcAbABadqnHrpfO
dPVT+T2C+2qA2zjIJ9a8ZfkH0LyGQH0V7jiainar+Q32ckVvdvMHnvkYrEztLsKl
heMVj1hvyKPEG9rbaoGm1LI/DWrK+lkkIq/KWBLOvkFoGu0avXK6CC1JTtIAnPni
ziRtnQVXBF1VycMBDADa2x0PZqwIRH2B8kw+CW1TMHe1nqotX1HYL809L+/bGK82
PsB1URKXedB5ELi//jYNp3mZTQjeJdhBvr2mlwmNmnr6hKMDtbSE0p4ouIWpDPUH
wYzhtapDdzOk0Ugia2LDZ21H01BS0LuemzhGXAMDeuYpsJt5mXkUQSf2qVdKJ72c
QiDCG3vtt2Tk/TD3HwftVEHHGphyH5365afEWT2XJVm5dCQZAXzEdZZLrnaFmc8O
a5tJK3AfXPkilRLdejGybs58WR4hndSg1W/5x7Gg9RWZG3UVS18RLlVQGtGdK7nj
COcIvxBRp2hoNnVSXPbbuPH1FIoVee8/4Oo52KFG6J+d8VAZp89Lwx0WGsyCGDN5
7mr8ekTb8q5PDSil6b39b1Am0ptjReGBoTR05+lU8LIjLlfBsLzwhCpF855zhBXh
gq65TSwOYWAvD5HZtip/29ai3nD/VmwnM0YiXNGE5C+BhKYFEz7R9douG2Irr834
NfHMClEjrNmy6+PCLdMAEQEAAQAL9iqaJzitmSSthcDwlDwp7vNtUTWJgpb9IcbI
3Krh1KRRqcm6wrwTjArwgM8QR5EYFcLoAZkAkI6tz0BSYO3iI3ntGFirzmUVJI62
cRMm2DM60nfatWc6db+sSeFLhpSB/Y0MFQ8Q6LyLj/olPPnKmiDooOUnURyFQ4yD
Im8MMnG89VagM2rq7rTXfkxqUkhzQe0bpFzy+w88GFnpWpReIB8hUNXzmxNDDDEy
B+UI5l7GEgg8lNMpdtSkGdshfwqd6QfLdTPXGM3bOY0XLt6CymimisXsfG+br2SK
N1BmrBXUZQ37NGyuI6HB20Buy7yigHbnpkD2xWcToBaNVcy67mJCrht9T59yQfJm
EzIxANlVfHlcyMergwu93apBQZaSeJDV/Wav5DzhnbZsBSUNSLCDkZa7+dhNQJPh
Yq7yrLDsNEztp5dTNwOvP32lgFbEE82lrrDrXzvuK2kOdPd96t0eBy69bOD7V9bT
QDrDFAF0/HcrcO9PgmK6NvVaK7+tBgDlmkV5c1RR1lABC3av0qLe2HSOtdimOjUp
A9RKOn7zV9jOkemKtFF1/u0mBZ8IZ/qgLEnp/z2BtxYOA67PpC6u1ahmyKlBf3Kp
b2yycQOqcGxYov2zl4AQOTcfj6UhMvgc5Gbba0faQ8kbPpb+yZKT9mg9wG/Tz4G4
nEw7IzvnH9ehFRqRrMqqjnAWJ9dQe8bTSYW0xqAP3wkzpW5KWKKms/qTVbA1fx7c
EYDjXo3zE1ZPQkbMUPmdY3elvKTgPqUGAPQEirzfHPn5Bz1rcjfWRRxQQK711M55
2kbnKZX4Xd0wcznq9I7mdVXEy7G4ugT2U+j6hPsW4j02SjioivIAvxCrCd/QBwQ2
rM7m10l3GfkGzhwUKOoVfzEqFFjSrkcYExTn8zUXDbayXUTMcXQJTPOswvJtksJ/
+5+t1gJAa0CMKkTiQCYEsdcRzc4aSLzwVMNkJ86/TgGa/CvB5WxmYh/vsT/bWxZD
W/UG/WYRuSkE9sYeGcQ1xq1Pt5Jn8inJFwX6Ak7mTEpcXktMLs6Jy0jdQJU7TQY+
2Muwbge+Q2xKqiJh/wmrCp53pesp1zKkRVD52qeeoyY+qJmEWb0iocMjeF9wNINX
WLUIOCyzx6pltNLcqbCaTyCcl8LT/W8KJ8qlP+5keh6moBiXWyFvRtKU1mL/Whuv
vyEd2Kp+DQv/lE+fFCTmd2sSpY048Hy0p4XU7JHDTpaoKl9pWQVang87tIYSNR0Q
3D1UFhhuPHvTBK0KtVhhMn32eocjasiwUVEk28KJAbwEGAEKACYWIQT1GSDCe1wl
LzFiIl3LIEh+NXx5ZgUCXVXJwwIbDAUJA8JnAAAKCRDLIEh+NXx5ZpUyDACjNFq7
gowgebEHW6N2HiDg+ejfH5kht1pjbeJaVqPeOMV1PbUXSU1hFsy2NZ/d+RmM8+id
YcTkERbPWYkC3mjIPURuXd0aBMhi3f69OInbC0xpS/JBR4C9J2PUvVEJpO1hT1/b
V6Y1eVvBsh86QlQzRdc9vRPxvLa17d4LlKZI6K2hyaMZdZ12spu/onSJUw+lzZ4H
1olOuIPeDq9TFoBekv0MfIkeX5v8HscAdadKlTl8Nmv2y/oinPP4/qLqA1Gm+AH4
5ap/LQvl6pRpnQcJLGs0ifov52B3q9n8h0+m95y5w4Z5ImfegrtKUWDPMbz7aeZP
Pzemld6RnxwyGQePZaLUAcdMJr89AkmZ+P9bN55i8y1Z+Qr9Rc5W3kkZEokK7LZh
bEJwKVbNNZyM5yHNzg9o3BxVfxwP5AQFqgkekipOmd363xRhL6uJCHgn1qZNteFc
+buLweqEJTE7fXHJxSUqBg/Xgs920S2CPlbQVMOG4b2fQAKS1KeowZX19Vg=
=bX39
-----END PGP PRIVATE KEY BLOCK-----
`
)

func diffStr(a, b string) string {
	al := strings.Split(strings.Replace(a, "\r", "", -1), "\n")
	bl := strings.Split(strings.Replace(b, "\r", "", -1), "\n")
	return cmp.Diff(al, bl)
}

func init() {
	message.RandomBoundaryFn = func() string {
		return "xxxxxx"
	}
	currentTimeFn = func() time.Time {
		return time.Date(1997, 11, 21, 9, 55, 6, 0, time.FixedZone("", -6*60*60))
	}
}
