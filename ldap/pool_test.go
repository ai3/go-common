package ldaputil

import (
	"context"
	"log"
	"net"
	"testing"
	"time"

	"github.com/go-ldap/ldap/v3"
	"github.com/go-asn1-ber/asn1-ber"
)

type tcpHandler interface {
	Handle(net.Conn)
}

type tcpHandlerFunc func(net.Conn)

func (f tcpHandlerFunc) Handle(c net.Conn) { f(c) }

// Base TCP server type (to build fake LDAP servers).
type tcpServer struct {
	l       net.Listener
	handler tcpHandler
}

func newTCPServer(t testing.TB, handler tcpHandler) *tcpServer {
	l, err := net.Listen("tcp", "localhost:0")
	if err != nil {
		t.Fatal("Listen():", err)
	}
	log.Printf("started new tcp server on %s", l.Addr().String())
	s := &tcpServer{l: l, handler: handler}
	go s.serve()
	return s
}

func (s *tcpServer) serve() {
	for {
		conn, err := s.l.Accept()
		if err != nil {
			return
		}
		go func(c net.Conn) {
			s.handler.Handle(c)
			c.Close()
		}(conn)
	}
}

func (s *tcpServer) Addr() string {
	return s.l.Addr().String()
}

func (s *tcpServer) Close() {
	s.l.Close()
}

// A test server that will close all incoming connections right away.
func newConnFailServer(t testing.TB) *tcpServer {
	return newTCPServer(t, tcpHandlerFunc(func(c net.Conn) {}))
}

// A test server that will close all connections after a 1s delay.
func newConnFailDelayServer(t testing.TB) *tcpServer {
	return newTCPServer(t, tcpHandlerFunc(func(c net.Conn) { time.Sleep(1 * time.Second) }))
}

// A fake LDAP server that will read a request and return a protocol error.
func newFakeBindOnlyLDAPServer(t testing.TB) *tcpServer {
	return newTCPServer(t, tcpHandlerFunc(func(c net.Conn) {
		var b [1024]byte
		c.Read(b[:]) // nolint: errcheck

		resp := ber.Encode(ber.ClassUniversal, ber.TypeConstructed, ber.TagSequence, nil, "LDAP Response")
		resp.AppendChild(ber.NewInteger(ber.ClassUniversal, ber.TypePrimitive, ber.TagInteger, 1, "MessageID"))
		resp.AppendChild(ber.NewSequence("Description"))

		c.Write(resp.Bytes()) // nolint: errcheck
	}))
}

func TestConnectionPool_ConnFail(t *testing.T) {
	runSearchQueries(t, newConnFailServer(t))
}

func TestConnectionPool_ConnFailDelay(t *testing.T) {
	runSearchQueries(t, newConnFailDelayServer(t))
}

func TestConnectionPool_PortClosed(t *testing.T) {
	srv := newConnFailServer(t)
	srv.Close()
	runSearchQueries(t, srv)
}

func TestConnectionPool_BindOnly(t *testing.T) {
	runSearchQueries(t, newFakeBindOnlyLDAPServer(t))
}

func runSearchQueries(t testing.TB, srv *tcpServer) {
	defer srv.Close()
	ldapURI := "ldap://" + srv.Addr()

	p, err := NewConnectionPool(ldapURI, "user", "password", 10)
	if err != nil {
		t.Fatal(err)
	}
	defer p.Close()

	for i := 0; i < 5; i++ {
		ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
		_, err := p.Search(ctx, ldap.NewSearchRequest(
			"o=Anarchy",
			ldap.ScopeWholeSubtree,
			ldap.NeverDerefAliases,
			0,
			0,
			false,
			"(objectClass=*)",
			[]string{"dn"},
			nil,
		))
		cancel()
		log.Printf("%d: %v", i, err)
		if err == nil {
			t.Error("weird, no error on Search")
		}
	}
}
