package compositetypes

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestAppSpecificPassword_Serialization(t *testing.T) {
	asp := &AppSpecificPassword{
		ID:                "abc",
		Service:           "service",
		EncryptedPassword: "$1$1234$5678abcdef",
		Comment:           "this: is a comment with a colon in it",
	}

	out, err := UnmarshalAppSpecificPassword(asp.Marshal())
	if err != nil {
		t.Fatalf("Unmarshal: %v", err)
	}
	if diffs := cmp.Diff(asp, out); diffs != "" {
		t.Fatalf("result differs: %s", diffs)
	}
}

func TestEncryptedKey_Serialization(t *testing.T) {
	key := &EncryptedKey{
		ID:           "main",
		EncryptedKey: []byte("this is a very secret key\x00"),
	}

	out, err := UnmarshalEncryptedKey(key.Marshal())
	if err != nil {
		t.Fatalf("Unmarshal: %v", err)
	}
	if diffs := cmp.Diff(key, out); diffs != "" {
		t.Fatalf("result differs: %s", diffs)
	}
}

func TestU2FRegistration_Serialization(t *testing.T) {
	key := &U2FRegistration{
		KeyHandle: []byte("\xc8\x1d\xd3\x9e~/\xb8\xedG(\xcb\x82\xf1\x0f\xb5\xac\xd6\xaf~\xc7\xfa\xb8\x96P\x91\xecJ\xa1,TRF\x88\xd1\x1a\xdaQ<\xd8-a\xd7\xb0\xd9v\xd7\xe8f\x8e\xab\xf6\x10\x895\xe6\x9f\xf3\x86;\xab\xc1\xae\x83^"),
		PublicKey: []byte("w'Ra\xd8\x17\xdf\x86\x06\xb0\xd0\x8f\x0eI\x98\xd7\xc1\xf7\xb0}j\xc3\x1c8\xf0\x8fh\xcf\xe0\x84W\xc6\xa3\x1d\xc8e/\xa5]v \xfa]\xa5\xfb\xd5c\xbe\xc72\xb9\x80\xa9\xc0O\xd1\xe5\x9d\xe0\xcd\x19q@\xeb\x04"),
	}

	out, err := UnmarshalU2FRegistration(key.Marshal())
	if err != nil {
		t.Fatalf("Unmarshal: %v", err)
	}
	if diffs := cmp.Diff(key, out); diffs != "" {
		t.Fatalf("result differs: %s", diffs)
	}
}

func TestU2FRegistration_Deserialization(t *testing.T) {
	// Sample key, obtained via "accounts" -> "accountserver" -> db.
	orig := "\x04\xe7r\x1a\x9b%e5\xb0\xa89\xcad9%\xd1wk\x059\xda?\xca\x13\xa3`\xa6H\x96Q\xa6\xa4$\x91C\xbf2r.\xa2\x1cz\xd2\xfc\xe4\xf9\x16\x02\x01\xb5\x04~Z\xf4\x02j-\xe18\xd4|+a;\x96\x08Wa\xe2sc9iu\xfb\x87\xb2\x99\xd0\xd4\x88!m\x8f\x94\xf6\xf8\x1d5\xf1\x1c\x9b\xce\xfbud;fj\x1b\x0fG\x8b\r\xa4\xae\xdai\x96N\x1bd\xb4\xff\xc0_T\x1f\xb3p\xdfT\xcb\x94\xc2\x0fb}\x1a"

	reg, err := UnmarshalU2FRegistration(orig)
	if err != nil {
		t.Fatalf("bad: %v", err)
	}
	if _, err := reg.Decode(); err != nil {
		t.Fatalf("decode: %v", err)
	}
}
