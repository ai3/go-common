module git.autistici.org/ai3/go-common

go 1.21.0

toolchain go1.23.1

require (
	github.com/NYTimes/gziphandler v1.1.1
	github.com/amoghe/go-crypt v0.0.0-20220222110647-20eada5f5964
	github.com/bbrks/wrap/v2 v2.5.0
	github.com/cenkalti/backoff/v4 v4.3.0
	github.com/coreos/go-systemd/v22 v22.5.0
	github.com/emersion/go-textwrapper v0.0.0-20200911093747-65d896831594
	github.com/fxamacker/cbor/v2 v2.7.0
	github.com/go-asn1-ber/asn1-ber v1.5.7
	github.com/go-ldap/ldap/v3 v3.4.8
	github.com/go-webauthn/webauthn v0.10.2
	github.com/gofrs/flock v0.12.1
	github.com/google/go-cmp v0.6.0
	github.com/lunixbochs/struc v0.0.0-20200707160740-784aaebc1d40
	github.com/mattn/go-sqlite3 v1.14.23
	github.com/miscreant/miscreant.go v0.0.0-20200214223636-26d376326b75
	github.com/prometheus/client_golang v1.20.3
	github.com/russross/blackfriday/v2 v2.1.0
	go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp v0.34.0
	go.opentelemetry.io/contrib/propagators/b3 v1.9.0
	go.opentelemetry.io/otel v1.10.0
	go.opentelemetry.io/otel/exporters/zipkin v1.9.0
	go.opentelemetry.io/otel/sdk v1.10.0
	go.opentelemetry.io/otel/trace v1.10.0
	golang.org/x/crypto v0.27.0
	golang.org/x/sync v0.8.0
)

require (
	github.com/Azure/go-ntlmssp v0.0.0-20221128193559-754e69321358 // indirect
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/felixge/httpsnoop v1.0.3 // indirect
	github.com/go-logr/logr v1.2.3 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/go-webauthn/x v0.1.9 // indirect
	github.com/golang-jwt/jwt/v5 v5.2.1 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-tpm v0.9.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/klauspost/compress v1.17.9 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/munnerz/goautoneg v0.0.0-20191010083416-a7dc8b61c822 // indirect
	github.com/openzipkin/zipkin-go v0.4.0 // indirect
	github.com/prometheus/client_model v0.6.1 // indirect
	github.com/prometheus/common v0.55.0 // indirect
	github.com/prometheus/procfs v0.15.1 // indirect
	github.com/x448/float16 v0.8.4 // indirect
	go.opentelemetry.io/otel/metric v0.31.0 // indirect
	golang.org/x/sys v0.25.0 // indirect
	google.golang.org/protobuf v1.34.2 // indirect
)
