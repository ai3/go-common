package clientutil

import (
	"context"
	"io"
	"log"
	"math/rand"
	"net"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"
)

type tcpHandler interface {
	Handle(net.Conn)
}

type tcpHandlerFunc func(net.Conn)

func (f tcpHandlerFunc) Handle(c net.Conn) { f(c) }

// Base TCP server type (to build fake LDAP servers).
type tcpServer struct {
	l       net.Listener
	handler tcpHandler
}

func newTCPServer(t testing.TB, handler tcpHandler) *tcpServer {
	l, err := net.Listen("tcp", "localhost:0")
	if err != nil {
		t.Fatal("Listen():", err)
	}
	log.Printf("started new tcp server on %s", l.Addr().String())
	s := &tcpServer{l: l, handler: handler}
	go s.serve()
	return s
}

func (s *tcpServer) serve() {
	for {
		conn, err := s.l.Accept()
		if err != nil {
			return
		}
		go func(c net.Conn) {
			s.handler.Handle(c)
			c.Close()
		}(conn)
	}
}

func (s *tcpServer) Addr() string {
	return s.l.Addr().String()
}

func (s *tcpServer) Close() {
	s.l.Close()
}

// A test server that will close all incoming connections right away.
func newConnFailServer(t testing.TB) *tcpServer {
	return newTCPServer(t, tcpHandlerFunc(func(c net.Conn) {}))
}

// A test server that will close all connections after a 1s delay.
func newConnFailDelayServer(t testing.TB) *tcpServer {
	return newTCPServer(t, tcpHandlerFunc(func(c net.Conn) { time.Sleep(1 * time.Second) }))
}

type httpServer struct {
	*httptest.Server
}

func (s *httpServer) Addr() string {
	u, _ := url.Parse(s.Server.URL)
	return u.Host
}

// An HTTP server that will always return a specific HTTP status using
// http.Error().
func newErrorHTTPServer(statusCode int) *httpServer {
	return &httpServer{httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Connection", "close")
		http.Error(w, "oh no", statusCode)
	}))}
}

func newJSONHTTPServer() *httpServer {
	return &httpServer{httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		io.WriteString(w, "{\"value\": 42}") // nolint
	}))}
}

func newHostCountingJSONHTTPServer() (*httpServer, map[string]int) {
	counters := make(map[string]int)
	return &httpServer{httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		counters[r.Host]++
		w.Header().Set("Content-Type", "application/json")
		io.WriteString(w, "{\"value\": 42}") // nolint
	}))}, counters
}

type testServer interface {
	Addr() string
	Close()
}

type testBackends struct {
	servers []testServer
	addrs   []string
}

func newTestBackends(servers ...testServer) *testBackends {
	b := new(testBackends)
	for _, s := range servers {
		b.servers = append(b.servers, s)
		b.addrs = append(b.addrs, s.Addr())
	}
	return b
}

func (b *testBackends) ResolveIP(_ string) []string {
	return b.addrs
}

func (b *testBackends) stop(i int) {
	b.servers[i].Close()
}

func (b *testBackends) close() {
	for _, s := range b.servers {
		s.Close()
	}
}

// Do a number of fake requests to a test JSONHTTPServer. If shards is
// not nil, set up a fake sharded service and pick one of the given
// shards randomly on every request.
func doJSONRequests(backends *testBackends, u string, n int, shards []string) (int, int) {
	b, err := newBalancedBackend(&BackendConfig{
		URL:     u,
		Debug:   true,
		Sharded: len(shards) > 0,
	}, backends)
	if err != nil {
		panic(err)
	}
	defer b.Close()

	var errs, oks int
	for i := 0; i < n; i++ {
		ctx, cancel := context.WithTimeout(context.Background(), 500*time.Millisecond)

		var resp struct {
			Value int `json:"value"`
		}
		var shard string
		if len(shards) > 0 {
			shard = shards[rand.Intn(len(shards))]
		}
		err = b.Call(ctx, shard, "/", struct{}{}, &resp)
		cancel()
		if err != nil {
			errs++
			log.Printf("request error: %v", err)
		} else if resp.Value != 42 {
			errs++
		} else {
			oks++
		}
	}

	return oks, errs
}

func TestBackend_TargetsDown(t *testing.T) {
	b := newTestBackends(newJSONHTTPServer(), newJSONHTTPServer(), newJSONHTTPServer())
	defer b.close()

	oks, errs := doJSONRequests(b, "http://test/", 10, nil)
	if errs > 0 {
		t.Fatalf("errs=%d", errs)
	}
	if oks == 0 {
		t.Fatal("oks=0")
	}

	// Stop the first two backends, request should still succeed.
	b.stop(0)
	b.stop(1)

	oks, errs = doJSONRequests(b, "http://test/", 10, nil)
	if errs > 0 {
		t.Fatalf("errs=%d", errs)
	}
	if oks < 10 {
		t.Fatalf("oks=%d", oks)
	}
}

func TestBackend_OverloadedTargets(t *testing.T) {
	b := newTestBackends(newErrorHTTPServer(http.StatusTooManyRequests), newJSONHTTPServer())
	defer b.close()

	oks, errs := doJSONRequests(b, "http://test/", 10, nil)
	if errs > 0 {
		t.Fatalf("errs=%d", errs)
	}
	if oks < 10 {
		t.Fatalf("oks=%d", oks)
	}
}

func TestBackend_BrokenTarget(t *testing.T) {
	b := newTestBackends(newConnFailServer(t), newJSONHTTPServer())
	defer b.close()

	oks, errs := doJSONRequests(b, "http://test/", 10, nil)
	if errs > 0 {
		t.Fatalf("errs=%d", errs)
	}
	if oks == 0 {
		t.Fatal("oks=0")
	}
}

func TestBackend_HighLatencyTarget(t *testing.T) {
	b := newTestBackends(newConnFailDelayServer(t), newJSONHTTPServer())
	defer b.close()

	oks, errs := doJSONRequests(b, "http://test/", 10, nil)
	// At most one request should fail (timing out).
	if errs > 1 {
		t.Fatalf("errs=%d", errs)
	}
	if oks == 0 {
		t.Fatal("oks=0")
	}
}

func TestBackend_Sharded(t *testing.T) {
	srv, counters := newHostCountingJSONHTTPServer()
	b := newTestBackends(srv)
	defer b.close()

	// Make some requests to two different shards (simulated by a
	// single http server), and count the Host headers seen.
	shards := []string{"s1", "s2"}
	oks, errs := doJSONRequests(b, "http://test/", 10, shards)
	if errs > 0 {
		t.Fatalf("errs=%d", errs)
	}
	if oks == 0 {
		t.Fatal("oks=0")
	}

	for _, s := range shards {
		n := counters[s+".test"]
		if n == 0 {
			t.Errorf("no requests for shard %s", s)
		}
	}
}
