package clientutil

import "testing"

type fakeResolver struct {
	addrs    []string
	requests int
}

func (r *fakeResolver) ResolveIP(host string) []string {
	r.requests++
	return r.addrs
}

func TestDNSCache(t *testing.T) {
	r := &fakeResolver{addrs: []string{"1.2.3.4"}}
	c := newDNSCache(r)
	for i := 0; i < 5; i++ {
		addrs := c.ResolveIP("a.b.c.d")
		if len(addrs) != 1 {
			t.Errorf("ResolveIP returned bad response: %v", addrs)
		}
	}
	if r.requests != 1 {
		t.Errorf("cached resolver has wrong number of requests: %d, expecting 1", r.requests)
	}
}
