package userenckey

import (
	"bytes"
	"log"
	"testing"
)

func TestGenerateKey(t *testing.T) {
	pub, priv, err := GenerateKey()
	if err != nil {
		t.Fatal(err)
	}

	if !bytes.HasPrefix(pub, []byte("-----BEGIN PUBLIC KEY-----")) {
		t.Errorf("bad public key: %s", string(pub))
	}
	if priv == nil {
		t.Fatalf("no private key returned")
	}
	if len(priv.rawBytes) == 0 {
		t.Fatalf("private key is empty")
	}

	// Parse the key now, check PKCS8 PEM header.
	pem, err := priv.PEM()
	if err != nil {
		t.Fatalf("error parsing private key: %v", err)
	}
	if !bytes.HasPrefix(pem, []byte("-----BEGIN PRIVATE KEY-----")) {
		t.Fatalf("bad PEM private key: %s", string(pem))
	}
}

func TestEncryptDecrypt(t *testing.T) {
	pw := []byte("stracchino")
	// Don't need to use a real key as Encrypt/Decrypt are
	// agnostic with respect to the container content.
	key := &Key{[]byte("this is a very secret key")}

	enc, err := Encrypt(key, pw)
	if err != nil {
		t.Fatal("Encrypt():", err)
	}

	log.Printf("encrypted key: %q (%d bytes)", enc, len(enc))

	dec, err := Decrypt([][]byte{enc}, pw)
	if err != nil {
		t.Fatal("Decrypt():", err)
	}
	if !bytes.Equal(key.rawBytes, dec.rawBytes) {
		t.Fatalf("bad decrypted ciphertext: %v", dec)
	}
}
