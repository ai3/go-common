package userenckey

import (
	"bytes"
	"testing"
)

func TestArgon2AESSIV(t *testing.T) {
	pw := []byte("secret pw")
	encKey := []byte("secret encryption key")

	key, err := encryptArgon2AESSIV(encKey, pw)
	if err != nil {
		t.Fatal("encryptArgon2AESSIV", err)
	}

	out, err := key.decryptArgon2AESSIV(pw)
	if err != nil {
		t.Fatal("decryptArgon2AESSIV", err)
	}

	if !bytes.Equal(out, encKey) {
		t.Fatal("decryption failed")
	}
}
